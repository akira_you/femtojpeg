# femtojpeg

This is only "reduce" mode version of picojpeg (https://github.com/richgel999/picojpeg).

# recude mode?
It gets 8x8 downsampled jpeg image.It's fast.

# Why?
In some case of IoT cameara device(such as esp32camera),Camera module outputs jpg file 
and the edge device judges This image must be sent to cloud or not.

In this cace, small image of the jpeg is useful.

# How to use
Please see sample code (test.c). almost the same usage as picojpeg.
you need copy fjpeg_config.h.sample to fjpeg_config.h.

If you only need gray scale image, set FJPEG_OUTPUT_GRAY to 1.

