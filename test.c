#include "fjpeg.h"
#include <stdlib.h>
#include <stdio.h>

typedef unsigned char uint8;
typedef unsigned int uint;
#ifndef max
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif
static FILE *g_pInFile;
static uint g_nInFileSize;
static uint g_nInFileOfs;
unsigned char pjpeg_need_bytes_callback(unsigned char *pBuf, unsigned char buf_size, unsigned char *pBytes_actually_read, void *pCallback_data)
{
    uint n;
    pCallback_data;

    n = min(g_nInFileSize - g_nInFileOfs, buf_size);
    if (n && (fread(pBuf, 1, n, g_pInFile) != n))
        return FJPG_STREAM_READ_ERROR;
    *pBytes_actually_read = (unsigned char)(n);
    g_nInFileOfs += n;
    return 0;
}
int main()
{
    fjpeg_image_info_t image_info;
    uint8 status;
    uint decoded_width, decoded_height;
    g_pInFile = fopen("test.jpg", "rb");
    if (!g_pInFile)
        return -1;

    g_nInFileOfs = 0;

    fseek(g_pInFile, 0, SEEK_END);
    g_nInFileSize = ftell(g_pInFile);
    fseek(g_pInFile, 0, SEEK_SET);

    status = fjpeg_decode_init(&image_info, pjpeg_need_bytes_callback, NULL);

    if (status)
    {
        printf("pjpeg_decode_init() failed with status %u\n", status);
        if (status == FJPG_UNSUPPORTED_MODE)
        {
            printf("Progressive JPEG files are not supported.\n");
        }

        fclose(g_pInFile);
        return -1;
    }
    uint8 *pImage;
    decoded_width = image_info.m_MCUSPerRow * image_info.m_MCUWidth / 8;
    decoded_height = image_info.m_MCUSPerCol * image_info.m_MCUHeight / 8;
#if FJPEG_OUTPUT_GRAY == 0
    const int nofCh = image_info.m_comps;
#else
    const int nofCh = 1;
#endif
    int row_pitch = decoded_width * nofCh;
    pImage = (uint8 *)malloc(row_pitch * decoded_height);
    int row_blocks_per_mcu = image_info.m_MCUWidth >> 3;
    int col_blocks_per_mcu = image_info.m_MCUHeight >> 3;

    int mcu_x = 0;
    int mcu_y = 0;
    for (;;)
    {
        uint8 *pDst_row;

        status = fjpeg_decode_mcu();

        if (status)
        {
            if (status != FJPG_NO_MORE_BLOCKS)
            {
                printf("pjpeg_decode_mcu() failed with status %u\n", status);

                free(pImage);
                fclose(g_pInFile);
                return -1;
            }

            break;
        }

        if (mcu_y >= image_info.m_MCUSPerCol)
        {
            free(pImage);
            fclose(g_pInFile);
            return -1;
        }

        {
            pDst_row = pImage + mcu_y * col_blocks_per_mcu * row_pitch + mcu_x * row_blocks_per_mcu * nofCh;
            if (image_info.m_scanType == FJPG_GRAYSCALE)
            {
                *pDst_row = image_info.m_pMCUBufR[0];
            }
            else
            {
                uint y, x;
                for (y = 0; y < col_blocks_per_mcu; y++)
                {
                    uint src_ofs = (y * 2U);
                    for (x = 0; x < row_blocks_per_mcu; x++)
                    {
                        pDst_row[0] = image_info.m_pMCUBufR[src_ofs];
                        if (nofCh != 1)
                        {
                            pDst_row[1] = image_info.m_pMCUBufG[src_ofs];
                            pDst_row[2] = image_info.m_pMCUBufB[src_ofs];
                        }
                        pDst_row += nofCh;
                        src_ofs += 1;
                    }

                    pDst_row += row_pitch - 3 * row_blocks_per_mcu;
                }
            }
        }
        mcu_x++;
        if (mcu_x == image_info.m_MCUSPerRow)
        {
            mcu_x = 0;
            mcu_y++;
        }
    }
    if (nofCh == 1)
    {
        printf("P2\n");
        printf("%d %d\n", decoded_width, decoded_height);
        printf("255\n");
        uint8 *p = pImage;

        for (int y = 0; y < decoded_height; y++)
        {
            for (int x = 0; x < decoded_width; x++)
            {
                printf("%d ", (int)(*p));
                p++;
            }
            printf("\n");
        }
    }
    if (nofCh == 3)
    {
        printf("P3\n");
        printf("%d %d\n", decoded_width, decoded_height);
        printf("255\n");
        uint8 *p = pImage;

        for (int y = 0; y < decoded_height; y++)
        {
            for (int x = 0; x < decoded_width; x++)
            {
                for (int c = 0; c < 3; c++)
                {
                    printf("%d ", (int)(*p));
                    p++;
                }
            }
            printf("\n");
        }
    }
    return 0;
}